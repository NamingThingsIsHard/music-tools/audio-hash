# audio-hash
# Copyright (C) 2019  NamingThingsIsHard / music tools
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
import argparse
import hashlib
import logging
from collections import defaultdict
from functools import cmp_to_key
from pathlib import Path

import audioread as audioread


def main(algorithm, paths, group_dupes=False, dupes_first=False, dupes_only=False):
    log = logging.getLogger("audio-hash")
    hasher_class = getattr(hashlib, algorithm)
    log.info("Using hash algorithm: %s", algorithm)
    if group_dupes:
        group_dupes = defaultdict(list)
        log.info("Grouping dupes. This might take a while...")
    for path in paths:
        _hash_it(hasher_class, Path(path), group_dupes)

    if group_dupes:
        items = group_dupes.items()
        if dupes_first:
            # Dupes have at least 2 files
            def comparator(left, right):
                return len(right[1]) - len(left[1])

            items = sorted(items, key=cmp_to_key(comparator))
        for _hash, files in items:
            if dupes_only and len(files) > 1:
                _format_hash_paths(_hash, *files)


def _hash_it(hasher_class, file_path, collector=False):
    if file_path.is_dir():
        dirs = []
        # Handles the files of directories first
        for item in file_path.iterdir():
            if item.is_dir():
                dirs.append(item)
            else:
                try:
                    _hash_file(hasher_class, item, collector)
                except audioread.exceptions.NoBackendError:
                    pass

        for directory in dirs:
            _hash_it(hasher_class, directory, collector)
    else:
        try:
            _hash_file(hasher_class, file_path, collector)
        except audioread.exceptions.NoBackendError:
            pass


def _hash_file(hasher_class, file_path, collector=None):
    with audioread.audio_open(file_path) as audio_file:
        hasher = hasher_class()
        for buf in audio_file:
            hasher.update(buf)
        _hash = hasher.hexdigest()
        if isinstance(collector, defaultdict):
            collector[_hash].append(file_path)
        else:
            _format_hash_paths(_hash, file_path)


def _format_hash_paths(_hash, *paths):
    hash_wide = "".join(" " for i in enumerate(_hash))
    for i, path in enumerate(paths):
        if i == 0:
            h = _hash
        else:
            h = hash_wide
        print("%(hash)s %(path)s" % {
            "hash": h,
            "path": path
        })


if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO)

    parser = argparse.ArgumentParser(description="Outputs the hashes of given audiofiles")
    algorithms = list(hashlib.algorithms_available)
    algorithms.sort()
    parser.add_argument("-a", "--algorithm", help="Which hash algorithm to use",
                        default="md5" if "md5" in algorithms else algorithms[0],
                        choices=algorithms)
    parser.add_argument("-g", "--group-dupes", help="Group dupes together in the output",
                        action="store_true")
    parser.add_argument("--dupes-first", help="In conjunction with --group-dupes, dupes will be printed first",
                        action="store_true")
    parser.add_argument("--dupes-only", help="In conjunction with --group-dupes, unique files won't be printed",
                        action="store_true")
    parser.add_argument("audio_files", nargs="+")

    args = parser.parse_args()
    main(args.algorithm, args.audio_files, args.group_dupes,
         dupes_first=args.dupes_first,
         dupes_only=args.dupes_only
         )
